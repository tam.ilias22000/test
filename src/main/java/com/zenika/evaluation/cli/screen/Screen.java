package com.zenika.evaluation.cli.screen;

public interface Screen {

    static void affichageDesScreen(Screen firstScreen){
        Screen displayScreen = firstScreen;

        while(displayScreen != null){

            displayScreen = displayScreen.render();

            if (displayScreen != null){
                System.out.println("""
                        _________________
                        """);
            }
        }
    }

    Screen render();
}

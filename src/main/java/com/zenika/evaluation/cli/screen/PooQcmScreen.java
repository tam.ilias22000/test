package com.zenika.evaluation.cli.screen;


import com.zenika.evaluation.domain.Questions;
import com.zenika.evaluation.domain.Quizz;

import java.util.List;


public class PooQcmScreen implements Screen{
    Quizz quizz = new Quizz();

    String q1 = "Lequel des éléments suivants n’est pas un concept POO en Java?\n"
            + "(a)Héritage\n(b)Encapsulation\n(c)Polymorphisme\n(d)Compilation\n";
    String q2 = "Quand la surcharge de méthode est-elle déterminée?\n"
            + "(a)execution\n(b)compilation\n(c)codage\n(d)execution2\n";
    String q3 = "Quand la surcharge de méthode est-elle déterminée?\n"
            + "(a)execution\n(b)compilation\n(c)codage\n(d)execution2\n";

    List<Questions> questions = List.of(
            new Questions(q1,"a b"),
            new Questions(q2,"b c"),
            new Questions(q3,"c"));

    @Override
    public Screen render() {
        Quizz.takeAnswerAndGetScore(questions);

        return new MenuScreen();
    }
}

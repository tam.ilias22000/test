package com.zenika.evaluation.cli.screen;

import com.zenika.evaluation.cli.InputStudent;

public class MenuScreen implements Screen{
    @Override
    public Screen render() {
        System.out.println("""
                === QCM WayToLearnX ===
                                
                Quel QCM souhaitez vous réalisez ?
                   1 - Java - Programmation orientée objet
                   2 - Java - Les collections - partie 1
                   3 - Quitter l’application
                                
                """);
        System.out.println("Votre choix : ");

        String choice = InputStudent.getInstance().getStudentChoice();
        return switch (choice){
            case "1" -> new PooQcmScreen();
            case "2" -> new JavaCollection();
            case "3" -> new GoodByeScreen();
                default -> this;
        };
    }
}

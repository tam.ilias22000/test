package com.zenika.evaluation.domain;

import com.zenika.evaluation.cli.InputStudent;

import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class Questions {
    private String prompt;
    private String answer;

    public Questions() {

    }

    public Questions(String prompt, String answer) {
        this.prompt = prompt;
        this.answer = answer;
    }

    public String getPrompt() {
        return prompt;
    }

    public String getAnswer() {
        return answer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Questions questions = (Questions) o;
        return Objects.equals(prompt, questions.prompt) && Objects.equals(answer, questions.answer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(prompt, answer);
    }


}

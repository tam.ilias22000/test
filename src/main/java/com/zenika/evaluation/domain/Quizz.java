package com.zenika.evaluation.domain;

import com.zenika.evaluation.cli.InputStudent;
import com.zenika.evaluation.cli.TeacherContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Quizz {

    private List<Questions> questions = new ArrayList<>();


    public static void takeAnswerAndGetScore(List<Questions> questions){
        System.out.println("Votre choix de reponses:");

        int score = 0;
        for(int i = 0; i < questions.size(); i++ ){
            int numberOfQuestion = i + 1;
            System.out.println(numberOfQuestion+ ")" + questions.get(i).getPrompt());

            String answer = InputStudent.getInstance().getStudentChoice();
            if(answer.equals(questions.get(i).getAnswer())){
                score++;
            }
            System.out.println("votre score est de " + score + "/" + questions.size());
        }
    }
}

package com.zenika.evaluation.cli;

import com.zenika.evaluation.cli.screen.HelloScreen;
import com.zenika.evaluation.cli.screen.Screen;

public class CliApplication {
    public static void main(String[] args) {
        Screen.affichageDesScreen(new HelloScreen());
    }
}

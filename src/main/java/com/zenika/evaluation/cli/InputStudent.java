package com.zenika.evaluation.cli;

import java.util.Scanner;

public class InputStudent {

    private static final Scanner scanner = new Scanner(System.in);

    private static final InputStudent INSTANCE = new InputStudent();

    public static InputStudent getInstance(){
        return INSTANCE;
    }
    private InputStudent(){

    }

    public String getStudentChoice(){
        String choice = null;

        do{
            try{
                choice = scanner.nextLine();
            }catch (Exception e){
                System.out.println("choix invalide");
            }
        }while (choice == null);

        return  choice;
    }


}

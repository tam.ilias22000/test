package com.zenika.evaluation.cli.screen;

public class GoodByeScreen implements Screen{
    @Override
    public Screen render() {
        System.out.println("Au revoir et à très bientôt.");
        return null;
    }
}

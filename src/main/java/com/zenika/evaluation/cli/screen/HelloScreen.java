package com.zenika.evaluation.cli.screen;

public class HelloScreen implements Screen{
    @Override
    public Screen render() {
        System.out.println("""
                Bonjour,
                Bienvenue dans notre QCM:
                """);
        return new MenuScreen();
    }
}

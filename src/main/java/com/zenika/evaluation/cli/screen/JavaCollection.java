package com.zenika.evaluation.cli.screen;

import com.zenika.evaluation.cli.InputStudent;
import com.zenika.evaluation.cli.StudentContext;
import com.zenika.evaluation.cli.TeacherContext;
import com.zenika.evaluation.domain.Questions;
import com.zenika.evaluation.domain.Quizz;

import java.util.List;

public class JavaCollection implements Screen {

    String q1 = "1. Lequel de ces packages contient toutes les classes de collection?\n"
            + "(a)java.awt\n(b)java.net\n(c)java.util\n(d)java.lang\n";
    String q2 = "2. Laquelle de ces classes ne fait pas partie du framework collection en Java?\n"
            + "(a)queu\n(b)stack\n(c)array\n(d)map\n";
    String q3 = "3. Laquelle de ces interfaces ne fait pas partie du framework collection en Java?\n"
            + "(a)sortedMap\n(b)sortList\n(c)Map\n(d)list\n";
    String q4 = "4. Laquelle de ces méthodes supprime tous les éléments d’une collection?"
            + "(a)refresh()\n(b)delete()\n(c)reset()\n(d)clear()\n";
    String q5 = "5. Qu’est-ce que Collection en Java?"
            + "(a)un groupe d'objet\n(b)un groupe d'interface\n(c)un groupe de classe\n(d)aucune de ses reponse n'est vrai\n";


    List<Questions> questions = List.of(
            new Questions(q1,"a"),
            new Questions(q2,"b"),
            new Questions(q3,"c"),
            new Questions(q4,"c"),
            new Questions(q5,"d"));
    @Override
    public Screen render() {
        Quizz.takeAnswerAndGetScore(questions);

        return new MenuScreen();
    }
}
